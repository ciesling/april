----------------
--> SETTINGS <--
----------------
vim.g.mapleader = ' '
vim.opt.termguicolors = true
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.scrolloff = 3
vim.opt.sidescrolloff = 3
vim.opt.clipboard = "unnamedplus"
vim.opt.autoindent = true
vim.opt.cursorline = true
vim.opt.undofile = true
vim.opt.signcolumn = "yes"
vim.opt.mouse = 'a'
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.hlsearch = false
vim.opt.wrap = false
vim.opt.tabstop = 4 
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.opt.showmatch = true
vim.opt.incsearch = true 
vim.opt.syntax = "on" 
vim.opt.splitright = true
vim.opt.splitbelow = true
vim.opt.backup = false
vim.opt.swapfile = false
vim.cmd('set guicursor=a:ver100-blinkwait175-blinkon175-blinkoff175')
vim.cmd('highlight LineNrAbove guifg=#808080')
vim.cmd('highlight cursorlinenr guifg=#ffffff')
vim.cmd('highlight LineNrBelow guifg=#808080')
----------------
--> KEYBINDS <--
----------------

--> Zip Controls
vim.keymap.set('n', '<C-c>', 'zc')

--> Hold Ctrl and press vim keys to navigate splits
vim.keymap.set("n", "<C-h>", "<C-w>h")						-- control+h switches to left split
vim.keymap.set("n", "<C-l>", "<C-w>l")						-- control+l switches to right split
vim.keymap.set("n", "<C-j>", "<C-w>j")						-- control+j switches to bottom split
vim.keymap.set("n", "<C-k>", "<C-w>k")	

--> Blazingly fast way out of insert mode
vim.keymap.set("i", "kj", "<Esc>")
vim.keymap.set("i", "jk", "<Esc>")
vim.keymap.set("i", "jj", "<Esc>")

-- Resize vertically
vim.keymap.set("n", "<C-Up>", ":resize +2<CR>")
vim.keymap.set("n", "<C-Down>", ":resize -2<CR>")
-- Resize horizontally
vim.keymap.set("n", "<C-Left>", ":vertical resize +2<CR>")
vim.keymap.set("n", "<C-Right>", ":vertical resize -2<CR>")

-- Misc
vim.keymap.set('n', '<C-s>', '<cmd>write<cr>')
vim.keymap.set("n", "<C-x>", "<cmd>q<cr>")
vim.keymap.set("n", "<F3>", "<cmd>split<cr>")
vim.keymap.set("n", "<F4>", "<cmd>vsplit<cr>")
vim.keymap.set("n", "<Down>", "<C-d>")
vim.keymap.set("n", "<Left>", ":set nornu<CR>:set nonu<CR>")
vim.keymap.set("n", "<Right>", ":set rnu<CR>:set nu<CR>")

-------------
--> TO-DO <--
-------------

--> Need HELP! <--
vim.g.SnazzyTransparent = 0

